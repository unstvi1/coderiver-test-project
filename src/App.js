import React from 'react'
import { BackgroundAnimation } from './components/BackgroundAnimation/BackgroundAnimation'
import MainPage from './components/MainPage/MainPage'
export const App = () => {
  return (
    <>
      <MainPage />
      <BackgroundAnimation />
    </>
  )
}

