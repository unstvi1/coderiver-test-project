import React from 'react'
import Header from '../Header/Header'
import MovieDescription from '../MovieDescription/MovieDescription'

const MainPage = () => {
  return (
    <div style={{width: '90%', position: 'absolute', left: '0px', right: '0px', margin: '0 auto', zIndex:'1'}}>
        <Header/>
        <MovieDescription/>
    </div>
  )
}

export default MainPage