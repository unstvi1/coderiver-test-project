import React from 'react'
import ReactSwipe from 'react-swipe';
import './Slider.css'

const Slider = () => {

    let reactSwipeEl;

    return (
        <div >
            <div className='carouselControls'>
                <h3>popular this week</h3>
                <div>
                    <button onClick={() => reactSwipeEl.prev()}><img src="images/prev.png" alt="previos" /></button>
                    <button onClick={() => reactSwipeEl.next()}> <img src="images/next.png" alt="next" /></button>
                </div>

            </div>


            <div className='carouselContainer'>
                <ReactSwipe
                    className="carousel"
                    swipeOptions={{ continuous: false }}
                    ref={el => (reactSwipeEl = el)}
                >
                    <div style={{ marginTop: '13px' }}>
                        <img src="images/Rectangle1.jpeg" />
                        <img src="images/Rectangle2.png" />
                        <img src="images/Rectangle3.png" />
                        <img src="images/Rectangle4.png" />
                        <img src="images/Rectangle5.png" />
                        <img src="images/Rectangle6.png" />
                    </div>
                </ReactSwipe>
            </div>
        </div>
    )
}

export default Slider