import * as THREE from "three"
import { Canvas, extend, useFrame, useThree } from "@react-three/fiber"
import { Physics, useSphere } from "@react-three/cannon"
import {Effects as EffectComposer, useTexture} from "@react-three/drei"
import { SSAOPass } from "three-stdlib"


extend({ SSAOPass })



const rfs = THREE.MathUtils.randFloatSpread;
const sphereGeometry = new THREE.SphereGeometry(1, 32, 32);
const baubleMaterial = new THREE.MeshStandardMaterial({ color: "red", roughness: 0, envMapIntensity: 0.2, emissive: "#370037" })
const sphereCounter = 30;
let sphereGravity = true;


export const BackgroundAnimation = () => (
  

  <Canvas shadows dpr={[1, 2]} camera={{ position: [0, 0, 30], fov: 40, near: 1, far: 40 }} style={{zIndex: "0"}}>
    <ambientLight intensity={0.25} />
    
    <spotLight intensity={1} angle={0.2} penumbra={1} position={[30, 30, 30]} castShadow shadow-mapSize={[512, 512]} />
    <directionalLight intensity={5} position={[-10, -10, -10]} color="purple" />
    <Physics gravity={[0, 1, 0]} iterations={10}>
      <Clump />
    </Physics>
    <Effects />
  
  </Canvas>
)
window.addEventListener('click', function(){
  sphereGravity = !sphereGravity;
})
function Clump({ mat = new THREE.Matrix4(), vec = new THREE.Vector3()}) {
  const texture = useTexture("/images/texture.jpg")
  const [ref, api] = useSphere(() => ({ args: [1], mass: 1, angularDamping: 0.1, linearDamping: 0.65, position: [rfs(20), rfs(20), rfs(20)] }))
  useFrame(() => {
    for (let i = 0; i < sphereCounter; i++) {
      
      ref.current.getMatrixAt(i, mat)
      ref.current.wi
      if(sphereGravity){
        ref.current.rotation.x += 0.0001;
        ref.current.rotation.z += 0.0001;
        api.at(i).applyImpulse()
        api.at(i).applyForce(vec.setFromMatrixPosition(mat).normalize().multiplyScalar(-50).toArray(), [0, 0, 0])
      }else{
        api.at(i).applyForce(vec.setFromMatrixPosition(mat).normalize().multiplyScalar(5).toArray(), [0, 0, 0])
      }
    }
  })
  return <instancedMesh ref={ref} castShadow receiveShadow args={[null, null, sphereCounter]} position={[8,-1,0]} geometry={sphereGeometry} material={baubleMaterial} material-map={texture} />
}

function Effects() {
  const { scene, camera } = useThree()
  return (
    <EffectComposer>
      <sSAOPass args={[scene, camera, 100, 100]}  />
    </EffectComposer>
  )
}
