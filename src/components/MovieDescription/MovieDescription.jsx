import React from 'react'
import RateStars from '../RateStars/RateStars'
import Slider from '../Slider/Slider'
import './MovieDescription.css'

const MovieDescription = () => {
   
 
      
  return (
    <div className='MovieDescription' >
        <div className="category">
            <span className='element-animation'>Drama</span>
            <hr />
            <span>Thriller</span>
            <hr />
             <span>Supernatural</span>
        </div>
        <h1>Stranger Things</h1>
        <div className='specification'>
            <span>2019</span>
            <hr />
            <span>DIRECTOR: <mark> Shawn Levy</mark></span>
            <hr />
            <span>seasons:<mark> 3 (5 Episodes)</mark></span>
        </div>
        <p>
         In 1980s Indiana, a group of young friends witness supernatural forces and secret government exploits. As they search for answers, the children unravel a series of extraordinary mysteries.
        </p>
        <RateStars/>
        <div className='buttonsConatiner'>
            <button>Stream now <img src="images/playButton.png" alt="play Button"/></button>
            <button>All episodes</button>
        </div>
        <Slider/>
        <div className="ageLimit"><span>16+</span></div>
    </div>
  )
}

export default MovieDescription